
#include <windows.h>

#include <stdio.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2\opencv.hpp>
#include <math.h>
#include "utils.h"
#include "mycanny.hpp"



#define USE_COMPRESSIVE_TRACKER 0
#define USE_STRUCK_TRACKER 1


#if USE_COMPRESSIVE_TRACKER
#include "CompressiveTracker.h"
#endif


#if USE_STRUCK_TRACKER
#include "struck/Config.h"
#include "struck/Tracker.h"
#endif

#include "vld.h"

#define USE_VIDEO 1

/*#undef MIN
#undef MAX
#define MAX(a,b) ((a)<(b)?(b):(a))
#define MIN(a,b) ((a)>(b)?(b):(a))*/

void crop(IplImage* src,  IplImage* dest, CvRect rect) {
    cvSetImageROI(src, rect); 
    cvCopy(src, dest); 
    cvResetImageROI(src); 
}

struct Lane {
	Lane(){}
	Lane(CvPoint a, CvPoint b, float angle, float kl, float bl): p0(a),p1(b),angle(angle),
		votes(0),visited(false),found(false),k(kl),b(bl) { }

	CvPoint p0, p1;
	int votes;
	bool visited, found;
	float angle, k, b;
};

struct Status {
	Status():reset(true),lost(0){}
	ExpMovingAverage k, b;
	bool reset;
	int lost;
};



struct Tracker
{
   void init()
   {

   }

   void track()
   {

   }
};



#if USE_STRUCK_TRACKER
// read config file
std::string configPath = "config.txt";
struck::Config struck_conf(configPath);

/*if (conf.features.size() == 0)
{
	cout << "error: no features specified in config" << endl;
	return EXIT_FAILURE;
}*/
#endif


class MovingAverage
{
public:
	MovingAverage(int n)
	{
		m_i = 0;
		m_sz = 0;
		m_n = n;
		m_buffer = new double[m_n];
	}

	~MovingAverage()
	{
		if (m_buffer)
			delete[] m_buffer;
	}

	void add(double val)
	{
		assert(m_buffer && m_i >= 0 && m_i < m_n);
		m_buffer[m_i] = val;
		++m_i;
		++m_sz;
		m_i %= m_n;
	}

	double get()
	{
		int sz = MIN(m_n, m_sz);
		double mean = 0;

		for (int i = 0; i < sz; ++i)
		{
			mean += m_buffer[i];
		}

		if (sz)
			mean /= sz;

		return mean;
	}


private:
	int m_i;
	int m_n;
	int m_sz;
	double * m_buffer;
};



struct Vehicle {
	CvPoint bmin, bmax;
	int symmetryX;
	bool valid;
	unsigned int lastUpdate;
	int max_h_line_length;
	int mean_h_line_length;
	MovingAverage * ma_max_h_line_length;
	MovingAverage * ma_mean_h_line_length;

#if USE_COMPRESSIVE_TRACKER
	cv::Rect rect;
	CompressiveTracker tracker;
#endif

#if USE_STRUCK_TRACKER
	struck::Tracker * struck_tracker;

	Vehicle()
		: struck_tracker(new struck::Tracker(struck_conf))
		, ma_max_h_line_length(0)
		, ma_mean_h_line_length(0)
	{

	}
	virtual ~Vehicle()
	{
		if (struck_tracker)
			delete struck_tracker;

		if (ma_max_h_line_length)
			delete ma_max_h_line_length;
		if (ma_mean_h_line_length)
			delete ma_mean_h_line_length;
	}
#endif

	void init_moving_average_length(int n)
	{
		ma_max_h_line_length = new MovingAverage(n);
		ma_mean_h_line_length = new MovingAverage(n);
	}

	void add_max_h_line_length()
	{
		assert(ma_max_h_line_length);		
		ma_max_h_line_length->add(max_h_line_length);
	}

	void add_mean_h_line_length()
	{
		assert(ma_mean_h_line_length);
		ma_mean_h_line_length->add(mean_h_line_length);
	}
};

struct VehicleSample {
	CvPoint center;
	float radi;
	unsigned int frameDetected;
	int vehicleIndex;
};




#define GREEN CV_RGB(0,255,0)
#define RED CV_RGB(255,0,0)
#define BLUE CV_RGB(255,0,255)
#define PURPLE CV_RGB(255,0,255)

Status laneR, laneL;
std::vector<Vehicle*> vehicles;
//std::vector<VehicleSample> samples;
std::list<VehicleSample> samples;

enum{
    SCAN_STEP = 5,			  // in pixels
	LINE_REJECT_DEGREES = 10, // in degrees
    BW_TRESHOLD = 250,		  // edge response strength to recognize for 'WHITE'
    BORDERX = 10,			  // px, skip this much from left & right borders
	MAX_RESPONSE_DIST = 5,	  // px
	
	CANNY_MIN_TRESHOLD = 1,	  // edge detector minimum hysteresis threshold
	CANNY_MAX_TRESHOLD = 100, // edge detector maximum hysteresis threshold

	HOUGH_TRESHOLD = 50,		// line approval vote threshold
	HOUGH_MIN_LINE_LENGTH = 50,	// remove lines shorter than this treshold
	HOUGH_MAX_LINE_GAP = 100,   // join lines to one with smaller than this gaps

	//CAR_DETECT_LINES = 4,    // minimum lines for a region to pass validation as a 'CAR'
	//CAR_H_LINE_LENGTH = 10,  // minimum horizontal line length from car body in px

	//MAX_VEHICLE_SAMPLES = 30,      // max vehicle detection sampling history
	//CAR_DETECT_POSITIVE_SAMPLES = MAX_VEHICLE_SAMPLES-2, // probability positive matches for valid car
	//MAX_VEHICLE_NO_UPDATE_FREQ = 15 // remove car after this much no update frames
};

std::string config_file = "config.xml";

double dainger_ma_max_h_line_length = 45;
double dainger_ma_mean_h_line_length = 30;
int CAR_DETECT_LINES = 3;    // minimum lines for a region to pass validation as a 'CAR'
int CAR_H_LINE_LENGTH = 10;  // minimum horizontal line length from car body in px
double CAR_H_LINE_LENGTH_PART = 0.2; // minimum horizontal line length from car body in part of detected window

int MAX_VEHICLE_SAMPLES = 30;      // max vehicle detection sampling history
int CAR_DETECT_POSITIVE_SAMPLES = MAX_VEHICLE_SAMPLES - 2; // probability positive matches for valid car
int MAX_VEHICLE_NO_UPDATE_FREQ = 120; // remove car after this much no update frames


double resize_factor = 2;
double resize_factor_2 = resize_factor * 2;
double track_rectangle_height_part = 0.35;
double track_rectangle_width_part = 0.4;
int moving_average_length = 5;

double haar_detector_scale_factor = 1.035; // every iteration increases scan window by 5%
int haar_detector_min_neighbours = 3; // minus 1, number of rectangles, that the object consists of

bool cycle = false;
int cameraIndex = 0;


void saveConfig()
{
	printf("saveConfig(%s)\n", config_file.c_str());

	CvFileStorage* fs = cvOpenFileStorage(config_file.c_str(), 0, CV_STORAGE_WRITE);


	cvWriteReal(fs, "haar_detector_scale_factor", haar_detector_scale_factor);
	cvWriteInt(fs, "haar_detector_min_neighbours", haar_detector_min_neighbours);


	cvWriteReal(fs, "dainger_ma_max_h_line_length", dainger_ma_max_h_line_length);
	cvWriteReal(fs, "dainger_ma_mean_h_line_length", dainger_ma_mean_h_line_length);
	cvWriteInt(fs, "CAR_DETECT_LINES", CAR_DETECT_LINES);
	cvWriteInt(fs, "CAR_H_LINE_LENGTH", CAR_H_LINE_LENGTH);
	cvWriteReal(fs, "CAR_H_LINE_LENGTH_PART", CAR_H_LINE_LENGTH_PART);


	cvWriteInt(fs, "MAX_VEHICLE_SAMPLES", MAX_VEHICLE_SAMPLES);
	cvWriteInt(fs, "CAR_DETECT_POSITIVE_SAMPLES", CAR_DETECT_POSITIVE_SAMPLES);
	cvWriteInt(fs, "MAX_VEHICLE_NO_UPDATE_FREQ", MAX_VEHICLE_NO_UPDATE_FREQ);

	cvWriteReal(fs, "resize_factor", resize_factor);
	cvWriteReal(fs, "track_rectangle_height_part", track_rectangle_height_part);
	cvWriteReal(fs, "track_rectangle_width_part", track_rectangle_width_part);

	cvWriteInt(fs, "moving_average_length", moving_average_length);
	cvWriteInt(fs, "cycle", cycle);
	cvWriteInt(fs, "cameraIndex", cameraIndex);

	cvReleaseFileStorage(&fs);
}

void loadConfig()
{
	CvFileStorage* fs = cvOpenFileStorage(config_file.c_str(), 0, CV_STORAGE_READ);

	haar_detector_scale_factor = cvReadRealByName(fs, 0, "haar_detector_scale_factor", haar_detector_scale_factor);
	haar_detector_min_neighbours = cvReadIntByName(fs, 0, "haar_detector_min_neighbours", haar_detector_min_neighbours);


	dainger_ma_max_h_line_length = cvReadRealByName(fs, 0, "dainger_ma_max_h_line_length", dainger_ma_max_h_line_length);
	dainger_ma_mean_h_line_length = cvReadRealByName(fs, 0, "dainger_ma_mean_h_line_length", dainger_ma_mean_h_line_length);
	CAR_DETECT_LINES = cvReadIntByName(fs, 0, "CAR_DETECT_LINES", CAR_DETECT_LINES);
	CAR_H_LINE_LENGTH = cvReadIntByName(fs, 0, "CAR_H_LINE_LENGTH", CAR_H_LINE_LENGTH);
	CAR_H_LINE_LENGTH_PART = cvReadRealByName(fs, 0, "CAR_H_LINE_LENGTH_PART", CAR_H_LINE_LENGTH_PART);

	MAX_VEHICLE_SAMPLES = cvReadIntByName(fs, 0, "MAX_VEHICLE_SAMPLES", MAX_VEHICLE_SAMPLES);
	CAR_DETECT_POSITIVE_SAMPLES = cvReadIntByName(fs, 0, "CAR_DETECT_POSITIVE_SAMPLES", CAR_DETECT_POSITIVE_SAMPLES);
	MAX_VEHICLE_NO_UPDATE_FREQ = cvReadIntByName(fs, 0, "MAX_VEHICLE_NO_UPDATE_FREQ", MAX_VEHICLE_NO_UPDATE_FREQ);


	resize_factor = cvReadRealByName(fs, 0, "resize_factor", resize_factor);
	track_rectangle_height_part = cvReadRealByName(fs, 0, "track_rectangle_height_part", track_rectangle_height_part);
	track_rectangle_width_part = cvReadRealByName(fs, 0, "track_rectangle_width_part", track_rectangle_width_part);

	moving_average_length = cvReadIntByName(fs, 0, "moving_average_length", moving_average_length);
	cycle = cvReadIntByName(fs, 0, "cycle", cycle);
	cameraIndex = cvReadIntByName(fs, 0, "cameraIndex", cameraIndex);

	cvReleaseFileStorage(&fs);
}


#define K_VARY_FACTOR 0.2f
#define B_VARY_FACTOR 20
#define MAX_LOST_FRAMES 30

void FindResponses(IplImage *img, int startX, int endX, int y, std::vector<int>& list)
{
    // scans for single response: /^\_

	const int row = y * img->width * img->nChannels;
	unsigned char* ptr = (unsigned char*)img->imageData;

    int step = (endX < startX) ? -1: 1;
    int range = (endX > startX) ? endX-startX+1 : startX-endX+1;

    for(int x = startX; range>0; x += step, range--)
    {
        if(ptr[row + x] <= BW_TRESHOLD) continue; // skip black: loop until white pixels show up

        // first response found
        int idx = x + step;

        // skip same response(white) pixels
        while(range > 0 && ptr[row+idx] > BW_TRESHOLD){
            idx += step;
            range--;
        }

		// reached black again
        if(ptr[row+idx] <= BW_TRESHOLD) {
            list.push_back(x);
        }

        x = idx; // begin from new pos
    }
}

unsigned char pixel(IplImage* img, int x, int y) {
	return (unsigned char)img->imageData[(y*img->width+x)*img->nChannels];
}

int findSymmetryAxisX(IplImage* half_frame, CvPoint bmin, CvPoint bmax) {
  
  float value = 0;
  int axisX = -1; // not found
  
  int xmin = bmin.x;
  int ymin = bmin.y;
  int xmax = bmax.x;
  int ymax = bmax.y;
  int half_width = half_frame->width/2;
  int maxi = 1;

  for(int x=xmin, j=0; x<xmax; x++, j++) {
	float HS = 0;
    for(int y=ymin; y<ymax; y++) {
		int row = y*half_frame->width*half_frame->nChannels;
        for(int step=1; step<half_width; step++) {
          int neg = x-step;
          int pos = x+step;
		  unsigned char Gneg = (neg < xmin) ? 0 : (unsigned char)half_frame->imageData[row+neg*half_frame->nChannels];
          unsigned char Gpos = (pos >= xmax) ? 0 : (unsigned char)half_frame->imageData[row+pos*half_frame->nChannels];
          HS += abs(Gneg-Gpos);
        }
    }

	if (axisX == -1 || value > HS) { // find minimum
		axisX = x;
		value = HS;
	}
  }

  return axisX;
}

bool hasVertResponse(IplImage* edges, int x, int y, int ymin, int ymax) {
	bool has = (pixel(edges, x, y) > BW_TRESHOLD);
	if (y-1 >= ymin) has &= (pixel(edges, x, y-1) < BW_TRESHOLD);
	if (y+1 < ymax) has &= (pixel(edges, x, y+1) < BW_TRESHOLD);
	return has;
}

int horizLine(IplImage* edges, int x, int y, CvPoint bmin, CvPoint bmax, int maxHorzGap) {

	// scan to right
	int right = 0;
	int gap = maxHorzGap;
	for (int xx=x; xx<bmax.x; xx++) {
		if (hasVertResponse(edges, xx, y, bmin.y, bmax.y)) {
			right++;
			gap = maxHorzGap; // reset
		} else {
			gap--;
			if (gap <= 0) {
				break;
			}
		}
	}

	int left = 0;
	gap = maxHorzGap;
	for (int xx=x-1; xx>=bmin.x; xx--) {
		if (hasVertResponse(edges, xx, y, bmin.y, bmax.y)) {
			left++;
			gap = maxHorzGap; // reset
		} else {
			gap--;
			if (gap <= 0) {
				break;
			}
		}
	}

	return left+right;
}

int vehicleValid(IplImage* half_frame, IplImage* edges, Vehicle* v) {
   // first step: find horizontal symmetry axis
   v->symmetryX = findSymmetryAxisX(half_frame, v->bmin, v->bmax);
   if (v->symmetryX == -1)
   {
      printf("v->symmetryX = %d\n", v->symmetryX);
      return -1;
   }

   // second step: cars tend to have a lot of horizontal lines
   int hlines = 0;
   int delta_x = v->bmax.x - v->bmin.x;
   int car_h_line_length = MIN(CAR_H_LINE_LENGTH, CAR_H_LINE_LENGTH_PART * delta_x);
   int max_h_line_length = 0;
   int mean_h_line_length = 0;
   for (int y = v->bmin.y; y < v->bmax.y; y++) {
      int h_line_length = horizLine(edges, v->symmetryX, y, v->bmin, v->bmax, 2);
      //if (h_line_length)
      //   printf ("h_line_length = %d delta_x = %d car_h_line_lenght=%d\n", h_line_length, delta_x, car_h_line_length);
      if (h_line_length >= car_h_line_length) {
         #if _DEBUG
         //cvCircle(half_frame, cvPoint(v->symmetryX, y), 2, PURPLE);
         #endif
         hlines++;

		 if (max_h_line_length < h_line_length)
			 max_h_line_length = h_line_length;

		 mean_h_line_length += h_line_length;
      }
   }

   if (hlines)
	   mean_h_line_length /= hlines;

   v->max_h_line_length = max_h_line_length;
   v->mean_h_line_length = mean_h_line_length;



   if (hlines >= CAR_DETECT_LINES)
      return +1;

   printf("\thlines = %d\n", hlines);

   return 0;
}

bool vehicleValid(IplImage* half_frame, IplImage* edges, Vehicle* v, int& index) {

   int rc = vehicleValid(half_frame, edges, v);

   if (-1 == rc)
      return false;

	index = -1;
	int midy = (v->bmax.y + v->bmin.y)/2;

	// third step: check with previous detected samples if car already exists
	int numClose = 0;
	float closestDist = 0;
#if 0
	for (int i = 0; i < samples.size(); i++) {
		int dx = samples[i].center.x - v->symmetryX;
		int dy = samples[i].center.y - midy;
		float Rsqr = dx*dx + dy*dy;
		
		if (Rsqr <= samples[i].radi*samples[i].radi) {
			numClose++;
			if (index == -1 || Rsqr < closestDist) {
				index = samples[i].vehicleIndex;
				closestDist = Rsqr;
			}
		}
	}
#else
	std::list<VehicleSample>::iterator it = samples.begin();
	std::list<VehicleSample>::iterator end = samples.end();
	for (; it != end; ++it) {
		int dx = (*it).center.x - v->symmetryX;
		int dy = (*it).center.y - midy;
		float Rsqr = dx*dx + dy*dy;

		if (Rsqr <= (*it).radi*(*it).radi) {
			numClose++;
			if (index == -1 || Rsqr < closestDist) {
				index = (*it).vehicleIndex;
				closestDist = Rsqr;
			}
		}
	}
#endif

	printf("\tnumClose = %d\n", numClose);

	return (1 == rc || numClose >= CAR_DETECT_POSITIVE_SAMPLES);
}

void removeOldVehicleSamples(unsigned int currentFrame) {
	// statistical sampling - clear very old samples
#if 0
	std::vector<VehicleSample> sampl;
	for (int i = 0; i < samples.size(); i++) {
		if (currentFrame - samples[i].frameDetected < MAX_VEHICLE_SAMPLES) {
			sampl.push_back(samples[i]);
		}
	}
	samples = sampl;
#else
	std::list<VehicleSample>::iterator it = samples.begin();
	std::list<VehicleSample>::iterator end = samples.end();
	for (; it != end; ) {
		if (currentFrame - (*it).frameDetected >= MAX_VEHICLE_SAMPLES) {
			it = samples.erase(it);
		}
		else
			++it;
	}
#endif
}

void removeSamplesByIndex(int index) {
#if 0
	std::vector<VehicleSample> sampl;
	for (int i = 0; i < samples.size(); i++) {
		if (samples[i].vehicleIndex != index) {
			sampl.push_back(samples[i]);
		}
	}
	samples = sampl;
#else
	std::list<VehicleSample>::iterator it = samples.begin();
	std::list<VehicleSample>::iterator end = samples.end();
	for (; it != end; ) {
		if ((*it).vehicleIndex == index)
		{
			it = samples.erase(it);
		}
		else
			++it;
	}

#endif
}

void removeLostVehicles(unsigned int currentFrame, IplImage* half_frame, IplImage* edges, IplImage* output_frame) {
	// remove old unknown/false vehicles & their samples, if any
	std::vector<Vehicle*>::iterator it = vehicles.begin();
	std::vector<Vehicle*>::iterator end = vehicles.end();
	for (int i = 0; it != end; ++it, ++i) {

		if (0 == (*it))
		{
			printf("assert!!!\n");
			assert(false);
			continue;
		}

		/*

		Detection and Tracking of Multiple, Partially Occluded Humans by Bayesian
		Combination of Edgelet based Part Detectors
		
		
		After a trajectory is initialized, an object is tracked by two
		strategies: data association and meanshift tracking. For a
		new frame, for all existing hypotheses, we first look for
		their corresponding detection responses in this frame. If
		there is a new detection response matched with a hypothesis
		H(v), then H(v) grows based on data association, otherwise
		a meanshift tracker is applied.
		*/

		// http://docs.opencv.org/3.0.0/db/df8/tutorial_py_meanshift.html

#if USE_COMPRESSIVE_TRACKER
      if ((*it)->valid && currentFrame == (*it)->lastUpdate)
      {
		  cv::Point center = cv::Point(((*it)->bmin.x + (*it)->bmax.x) / 2, ((*it)->bmin.y + (*it)->bmax.y) / 2);
		  int w = track_rectangle_part * ((*it)->bmax.x - (*it)->bmin.x);
		  int h = track_rectangle_part * ((*it)->bmax.y - (*it)->bmin.y);
		  
		  (*it)->rect.x = center.x - w / 2;
		  (*it)->rect.y = center.y - h / 2;
		  (*it)->rect.width = w;
		  (*it)->rect.height = h;

		  (*it)->tracker.init(cv::Mat(half_frame), (*it)->rect);

		  cvRectangle(half_frame, (*it)->rect.tl(), (*it)->rect.br(), BLUE);
      }
      else if ((*it)->valid && currentFrame - (*it)->lastUpdate < MAX_VEHICLE_NO_UPDATE_FREQ)
      {
		  (*it)->tracker.processFrame(cv::Mat(half_frame), (*it)->rect);
		  cvRectangle(half_frame, (*it)->rect.tl(), (*it)->rect.br(), BLUE);

		  cv::Point center = cv::Point((*it)->rect.x + ((*it)->rect.width / 2), (*it)->rect.y + ((*it)->rect.height) / 2);
		  int W = (*it)->bmax.x - (*it)->bmin.x;
		  int H = (*it)->bmax.y - (*it)->bmin.y;


		  (*it)->bmin.x = center.x - W / 2;
		  (*it)->bmin.y = center.y - H / 2;

		  (*it)->bmax.x = (*it)->bmin.x + W;
		  (*it)->bmax.y = (*it)->bmin.y + H;


         /*if (1 != vehicleValid(half_frame, edges, (*it))) { 
            removeSamplesByIndex(i);
            (*it)->valid = false;
         }*/
      }
	  else 
#endif

#if USE_STRUCK_TRACKER
	  if ((*it)->valid && currentFrame == (*it)->lastUpdate)
		{
			struck::FloatRect initBB;

			cv::Point_<float> center = cv::Point_<float>(float((*it)->bmin.x + (*it)->bmax.x) / 2, float((*it)->bmin.y + (*it)->bmax.y) / 2);
			float w = track_rectangle_width_part * float((*it)->bmax.x - (*it)->bmin.x);
			float h = track_rectangle_height_part * float((*it)->bmax.y - (*it)->bmin.y);

			initBB.SetXMin ( center.x - w / 2);
			initBB.SetYMin ( center.y - h / 2);
			initBB.SetWidth(w);
			initBB.SetHeight(h);

			(*it)->struck_tracker->Initialise(cv::Mat(half_frame), initBB);

		}
		else if ((*it)->valid && currentFrame - (*it)->lastUpdate < MAX_VEHICLE_NO_UPDATE_FREQ)
		{
			if ((*it)->struck_tracker->IsInitialised())
			{
				(*it)->struck_tracker->Track(cv::Mat(half_frame));

				const struck::FloatRect& resBB = (*it)->struck_tracker->GetBB();

				cvRectangle(half_frame,
					cvPoint(resBB.XMin(), resBB.YMin()),
					cvPoint(resBB.XMax(), resBB.YMax()),
					BLUE);

				cvRectangle(output_frame,
					cvPoint(resize_factor_2 * resBB.XMin(), resize_factor_2 * resBB.YMin()),
					cvPoint(resize_factor_2 * resBB.XMax(), resize_factor_2 * resBB.YMax()),
					BLUE);

				cv::Point_<float> center = cv::Point_<float>(resBB.XCentre(), resBB.YCentre());
				float W = (*it)->bmax.x - (*it)->bmin.x;
				float H = (*it)->bmax.y - (*it)->bmin.y;

				(*it)->bmin.x = center.x - W / 2;
				(*it)->bmin.y = center.y - H / 2;

				(*it)->bmax.x = (*it)->bmin.x + W;
				(*it)->bmax.y = (*it)->bmin.y + H;

				if (1 == vehicleValid(half_frame, edges, (*it)))
				{
					(*it)->add_max_h_line_length();
					(*it)->add_mean_h_line_length();
				}
				else
				{
					removeSamplesByIndex(i);
					(*it)->valid = false;
					if ((*it)->struck_tracker->IsInitialised())
					{
						(*it)->struck_tracker->Reset();
					}

				}
			}
		}

		else
#endif

      if ((*it)->valid && currentFrame - (*it)->lastUpdate >= MAX_VEHICLE_NO_UPDATE_FREQ) {
			printf("\tremoving inactive car, index = %d\n", i);
			removeSamplesByIndex(i);
			(*it)->valid = false;
#if USE_STRUCK_TRACKER
			if ((*it)->struck_tracker->IsInitialised())
			{
				(*it)->struck_tracker->Reset();
			}
#endif
			
		}
	}

}

/*
Camera.Parameters parameters = mOpenCvCameraView.getCameraParameters();
float horizontalAngleOfView = parameters.getHorizontalViewAngle();
android.hardware.Camera.Size resolution = mOpenCvCameraView.getResolution();
double ratio = ((double)fig.width) / ((double)resolution.width);
double angle = horizontalAngleOfView * ratio / 2.0;
double d = 0.5 / (Math.tan(Math.toRadians(angle)));
*/

void vehicleDetection(IplImage* half_frame, IplImage* output_frame, CvHaarClassifierCascade* cascade, CvMemStorage* haarStorage) {

	static unsigned int frame = 0;
	frame++;
	printf("*** vehicle detector frame: %d ***\n", frame);

	removeOldVehicleSamples(frame);

	// Haar Car detection
	CvSeq* rects = cvHaarDetectObjects(half_frame, cascade, haarStorage, haar_detector_scale_factor, haar_detector_min_neighbours, CV_HAAR_DO_CANNY_PRUNING);

	// Canny edge detection of the minimized frame
      IplImage *grey = cvCreateImage(cvSize(half_frame->width, half_frame->height), IPL_DEPTH_8U, 1);
      IplImage *edges = cvCreateImage(cvSize(half_frame->width, half_frame->height), IPL_DEPTH_8U, 1);
      cvCvtColor(half_frame, grey, CV_BGR2GRAY); // convert to grayscale
	  //cvCanny(grey, edges, CANNY_MIN_TRESHOLD, CANNY_MAX_TRESHOLD);
	  cvMyCannyEx(grey, edges, 25., 12.5, 3);

      if (rects->total > 0) {
         printf("\thaar detected %d car hypotheses\n", rects->total);

		/* validate vehicles */
		for (int i = 0; i < rects->total; i++) {
			CvRect* rc = (CvRect*)cvGetSeqElem(rects, i);

			Vehicle* v = new Vehicle();
			v->bmin = cvPoint(rc->x, rc->y);
			v->bmax = cvPoint(rc->x + rc->width, rc->y + rc->height);
			v->valid = true;

			int index;
			if (vehicleValid(half_frame, edges, v, index)) { // put a sample on that position


				bool to_update = false;
				v->lastUpdate = frame;

				if (index == -1) { // new car detected

					// re-use already created but inactive vehicles
					for(int j=0; j<vehicles.size(); j++) {
						if (vehicles[j]->valid == false) {
							index = j;
							to_update = true;
							break;
						}

					}
					if (index == -1) { // all space used
						index = vehicles.size();

						v->init_moving_average_length(moving_average_length);
						v->add_max_h_line_length();
						v->add_mean_h_line_length();

						vehicles.push_back(v);
					}
					printf("\tnew car detected, index = %d\n", index);
				} else {
					// update the position from new data
					to_update = true;

					printf("\tcar updated, index = %d\n", index);

				}

				VehicleSample vs;
				vs.frameDetected = frame;
				vs.vehicleIndex = index;
				vs.radi = (MAX(rc->width, rc->height)) / 4; // radius twice smaller - prevent false positives
				vs.center = cvPoint((v->bmin.x + v->bmax.x) / 2, (v->bmin.y + v->bmax.y) / 2);
				samples.push_back(vs);

				if (to_update)
				{
					vehicles[index]->bmin.x = v->bmin.x;
					vehicles[index]->bmin.y = v->bmin.y;
					vehicles[index]->bmax.x = v->bmax.x;
					vehicles[index]->bmax.y = v->bmax.y;
					vehicles[index]->symmetryX = v->symmetryX;
					vehicles[index]->lastUpdate = frame;
					vehicles[index]->valid = true;

					vehicles[index]->max_h_line_length = v->max_h_line_length;
					vehicles[index]->mean_h_line_length = v->mean_h_line_length;

					vehicles[index]->add_max_h_line_length();
					vehicles[index]->add_mean_h_line_length();

					delete v;
				}


			}
			else
			{
				printf("\tvehicle %d is not validated\n", i);
				delete v;
			}
		}
      } else {
         printf("\tno vehicles detected in current frame!\n");
      }

      removeLostVehicles(frame, half_frame, edges, output_frame);
	  /*
	  for (int i = 0; i < rects->total; i++) {
		  CvRect* rc = (CvRect*)cvGetSeqElem(rects, i);

		  cvRectangle(half_frame,
			  cvPoint(rc->x, rc->y),
			  cvPoint(rc->x + rc->width, rc->y + rc->height),
			  CV_RGB(255, 0, 0), 3, 8, 0);
	  }*/


		cvShowImage("Half-frame[edges]", edges);
		//cvMoveWindow("Half-frame[edges]", half_frame->width*2+10, half_frame->height); 
      cvReleaseImage(&edges);
      cvReleaseImage(&grey);



	printf("\ttotal vehicles on screen: %d\n", vehicles.size());
}

void drawVehicles(IplImage* half_frame, IplImage* output_frame) {
	char text[128];
	CvFont font1 = cvFont(0.9, 1);
	CvFont font2 = cvFont(1.5, 1);

	// show vehicles
	for (int i = 0; i < vehicles.size(); i++) {
		Vehicle* v = vehicles[i];
		if (v && v->valid) {
			cvRectangle(half_frame, v->bmin, v->bmax, GREEN, 1);

			cvRectangle(output_frame, 
				cvPoint( resize_factor_2 * v->bmin.x, resize_factor_2 * v->bmin.y),
				cvPoint( resize_factor_2 * v->bmax.x, resize_factor_2 * v->bmax.y),
				GREEN, 1);

			int midY = (v->bmin.y + v->bmax.y) / 2;
			//cvLine(half_frame, cvPoint(v->symmetryX, midY-10), cvPoint(v->symmetryX, midY+10), PURPLE);
			double ma_max = v->ma_max_h_line_length->get();
			double ma_mean = v->ma_mean_h_line_length->get();
			sprintf_s(text, 128, "%d %0.1f %d %0.1f"
				, v->max_h_line_length
				, ma_max
				, v->mean_h_line_length
				, ma_mean
				);

			cvPutText(half_frame, text, cvPoint(v->bmin.x, v->bmax.y), &font1, GREEN);
			cvPutText(output_frame, text, cvPoint(resize_factor_2 * v->bmin.x, resize_factor_2 * v->bmax.y), &font2, GREEN);

			if (ma_mean > dainger_ma_mean_h_line_length)
			{
				printf("ma_mean = %0.1f > dainger_ma_mean_h_line_length %0.0f dainger!!!\n", ma_mean, dainger_ma_mean_h_line_length);
				if (!PlaySoundA("dainger.wav", NULL, SND_FILENAME | SND_NOSTOP | SND_ASYNC))
				{
				}
			}
			
			if (ma_max > dainger_ma_max_h_line_length)
			{
				printf("ma_max = %0.1f > dainger_ma_max_h_line_length %0.0f dainger!!!\n", ma_max, dainger_ma_max_h_line_length);
				if (!PlaySoundA("dainger.wav", NULL, SND_FILENAME | SND_NOSTOP | SND_ASYNC))
				{
				}
			}
		}
	}

	// show vehicle position sampling
	//for (int i = 0; i < samples.size(); i++) {
	//	cvCircle(half_frame, cvPoint(samples[i].center.x, samples[i].center.y), samples[i].radi, RED);
	//}
}



void freeVehicles() {
	for (int i = 0; i < vehicles.size(); i++) {
		Vehicle* v = vehicles[i];
		if (v)
			delete v;
	}
	vehicles.clear();
}

void processSide(std::vector<Lane> lanes, IplImage *edges, bool right) {

	Status* side = right ? &laneR : &laneL;

	// response search
	int w = edges->width;
	int h = edges->height;
	const int BEGINY = 0;
	const int ENDY = h-1;
	const int ENDX = right ? (w-BORDERX) : BORDERX;
	int midx = w/2;
	int midy = edges->height/2;
	unsigned char* ptr = (unsigned char*)edges->imageData;

	// show responses
	int* votes = new int[lanes.size()];
	for(int i=0; i<lanes.size(); i++) votes[i++] = 0;

	for(int y=ENDY; y>=BEGINY; y-=SCAN_STEP) {
		std::vector<int> rsp;
		FindResponses(edges, midx, ENDX, y, rsp);

		if (rsp.size() > 0) {
			int response_x = rsp[0]; // use first reponse (closest to screen center)

			float dmin = 9999999;
			float xmin = 9999999;
			int match = -1;
			for (int j=0; j<lanes.size(); j++) {
				// compute response point distance to current line
				float d = dist2line(
						cvPoint2D32f(lanes[j].p0.x, lanes[j].p0.y), 
						cvPoint2D32f(lanes[j].p1.x, lanes[j].p1.y), 
						cvPoint2D32f(response_x, y));

				// point on line at current y line
				int xline = (y - lanes[j].b) / lanes[j].k;
				int dist_mid = abs(midx - xline); // distance to midpoint

				// pick the best closest match to line & to screen center
				if (match == -1 || (d <= dmin && dist_mid < xmin)) {
					dmin = d;
					match = j;
					xmin = dist_mid;
					break;
				}
			}

			// vote for each line
			if (match != -1) {
				votes[match] += 1;
			}
		}
	}

	int bestMatch = -1;
	int mini = 9999999;
	for (int i=0; i<lanes.size(); i++) {
		int xline = (midy - lanes[i].b) / lanes[i].k;
		int dist = abs(midx - xline); // distance to midpoint

		if (bestMatch == -1 || (votes[i] > votes[bestMatch] && dist < mini)) {
			bestMatch = i;
			mini = dist;
		}
	}

	if (bestMatch != -1) {
		Lane* best = &lanes[bestMatch];
		float k_diff = fabs(best->k - side->k.get());
		float b_diff = fabs(best->b - side->b.get());

		bool update_ok = (k_diff <= K_VARY_FACTOR && b_diff <= B_VARY_FACTOR) || side->reset;

		printf("side: %s, k vary: %.4f, b vary: %.4f, lost: %s\n", 
			(right?"RIGHT":"LEFT"), k_diff, b_diff, (update_ok?"no":"yes"));
		
		if (update_ok) {
			// update is in valid bounds
			side->k.add(best->k);
			side->b.add(best->b);
			side->reset = false;
			side->lost = 0;
		} else {
			// can't update, lanes flicker periodically, start counter for partial reset!
			side->lost++;
			if (side->lost >= MAX_LOST_FRAMES && !side->reset) {
				side->reset = true;
			}
		}

	} else {
		printf("no lanes detected - lane tracking lost! counter increased\n");
		side->lost++;
		if (side->lost >= MAX_LOST_FRAMES && !side->reset) {
			// do full reset when lost for more than N frames
			side->reset = true;
			side->k.clear();
			side->b.clear();
		}
	}

	delete[] votes;
}

void processLanes(CvSeq* lines, IplImage* edges, IplImage* temp_frame) {

	// classify lines to left/right side
	std::vector<Lane> left, right;

	for(int i = 0; i < lines->total; i++ )
    {
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
		int dx = line[1].x - line[0].x;
		int dy = line[1].y - line[0].y;
		float angle = atan2f(dy, dx) * 180/CV_PI;

		if (fabs(angle) <= LINE_REJECT_DEGREES) { // reject near horizontal lines
			continue;
		}

		// assume that vanishing point is close to the image horizontal center
		// calculate line parameters: y = kx + b;
		dx = (dx == 0) ? 1 : dx; // prevent DIV/0!  
		float k = dy/(float)dx;
		float b = line[0].y - k*line[0].x;

		// assign lane's side based by its midpoint position 
		int midx = (line[0].x + line[1].x) / 2;
		if (midx < temp_frame->width/2) {
			left.push_back(Lane(line[0], line[1], angle, k, b));
		} else if (midx > temp_frame->width/2) {
			right.push_back(Lane(line[0], line[1], angle, k, b));
		}
    }

	// show Hough lines
	for	(int i=0; i<right.size(); i++) {
		cvLine(temp_frame, right[i].p0, right[i].p1, CV_RGB(0, 0, 255), 2);
	}

	for	(int i=0; i<left.size(); i++) {
		cvLine(temp_frame, left[i].p0, left[i].p1, CV_RGB(255, 0, 0), 2);
	}

	processSide(left, edges, false);
	processSide(right, edges, true);

	// show computed lanes
	int x = temp_frame->width * 0.55f;
	int x2 = temp_frame->width;
	cvLine(temp_frame, cvPoint(x, laneR.k.get()*x + laneR.b.get()), 
		cvPoint(x2, laneR.k.get() * x2 + laneR.b.get()), CV_RGB(255, 0, 255), 2);

	x = temp_frame->width * 0;
	x2 = temp_frame->width * 0.45f;
	cvLine(temp_frame, cvPoint(x, laneL.k.get()*x + laneL.b.get()), 
		cvPoint(x2, laneL.k.get() * x2 + laneL.b.get()), CV_RGB(255, 0, 255), 2);
}

int test_two_cameras()
{
	CvCapture *capture0 = cvCaptureFromCAM(0);
	CvCapture *capture1 = cvCaptureFromCAM(1);

	if (!capture0) return 1;
	if (!capture1) return 1;
	cvNamedWindow("Video0");
	cvNamedWindow("Video1");

	while (true) {
		//grab and retrieve each frames of the video sequentially 
		IplImage* frame0 = cvQueryFrame(capture0);
		IplImage* frame1 = cvQueryFrame(capture1);

		if (!frame0 || !frame1) break;

		cvShowImage("Video0", frame0);
		cvShowImage("Video1", frame1);

		//wait for 40 milliseconds
		int c = cvWaitKey(40);

		//exit the loop if user press "Esc" key  (ASCII value of "Esc" is 27) 
		if ((char)c == 27) break;
	}

	cvReleaseCapture(&capture0);
	cvReleaseCapture(&capture1);


	return 0;
}

int main(int argc, char **argv)
{
	loadConfig();	

	CvCapture * input_video = NULL;

	if (argc > 1)
		input_video = cvCreateFileCapture(argv[1]);
	else
		input_video = cvCaptureFromCAM(cameraIndex);

#ifdef USE_VIDEO	
	//CvCapture *input_video = cvCreateFileCapture("FCW.mp4");
	//CvCapture *input_video = cvCreateFileCapture("D:/_C++/stereo/testvideo/test.mp4");
	//CvCapture *input_video = cvCreateFileCapture("D:\\cpplibs\\bgs\\opencv-lane-vehicle-track\\opencv-lane-vehicle-track\\bin\\road.avi");
#else
	CvCapture *input_video = cvCaptureFromCAM(0);
#endif

	if (input_video == NULL) {
		fprintf(stderr, "Error: Can't open video\n");
		return -1;
	}


	resize_factor_2 = resize_factor * 2;

	CvFont font;
	cvInitFont( &font, CV_FONT_VECTOR0, 0.25f, 0.25f);

	CvSize video_size;
	video_size.height = (int) cvGetCaptureProperty(input_video, CV_CAP_PROP_FRAME_HEIGHT);
	video_size.width = (int) cvGetCaptureProperty(input_video, CV_CAP_PROP_FRAME_WIDTH);

	long current_frame = 0;
	int key_pressed = 0;
	IplImage *frame = NULL;

	frame = cvQueryFrame(input_video);
	int c = 0;
	while (!frame)
	{

		frame = cvQueryFrame(input_video);
		if (++c > 10)
		{
			std::cerr << "Cannot read frame" << std::endl;
			return 1;
		}
	}

	CvSize frame_size = cvSize(video_size.width, video_size.height/2);
	//IplImage *temp_frame = cvCreateImage(frame_size, IPL_DEPTH_8U, 3);
	//IplImage *grey = cvCreateImage(frame_size, IPL_DEPTH_8U, 1);
	//IplImage *edges = cvCreateImage(frame_size, IPL_DEPTH_8U, 1);

	IplImage *output_frame = cvCreateImage(cvSize(resize_factor * video_size.width, resize_factor * video_size.height), IPL_DEPTH_8U, 3);
	IplImage *half_frame = cvCreateImage(cvSize(video_size.width / 2, video_size.height / 2), IPL_DEPTH_8U, 3);

	CvMemStorage* houghStorage = cvCreateMemStorage(0);
	CvMemStorage* haarStorage = cvCreateMemStorage(0);
	CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad("haar/cars3.xml");

	int delay = 1;

	//cvSetCaptureProperty(input_video, CV_CAP_PROP_POS_FRAMES, current_frame);
	while(key_pressed != 27) {

		frame = cvQueryFrame(input_video);
		if (frame == NULL) {
			if (cycle)
			{
				/* Return to the beginning */
				cvSetCaptureProperty(input_video, CV_CAP_PROP_POS_FRAMES, 0.);

				frame = cvQueryFrame(input_video);
			}
			if (frame == NULL) {
				fprintf(stderr, "Error: null frame received\n");
				return -1;
			}
		}

		cvPyrDown(frame, half_frame, CV_GAUSSIAN_5x5); // Reduce the image by 2
		cvResize(frame, output_frame);

		//cvCvtColor(temp_frame, grey, CV_BGR2GRAY); // convert to grayscale
		/*
		// we're interested only in road below horizont - so crop top image portion off
		crop(frame, temp_frame, cvRect(0,frame_size.height,frame_size.width,frame_size.height));
		cvCvtColor(temp_frame, grey, CV_BGR2GRAY); // convert to grayscale
	
		// Perform a Gaussian blur ( Convolving with 5 X 5 Gaussian) & detect edges
		cvSmooth(grey, grey, CV_GAUSSIAN, 5, 5);
		cvCanny(grey, edges, CANNY_MIN_TRESHOLD, CANNY_MAX_TRESHOLD);

		// do Hough transform to find lanes
		double rho = 1;
		double theta = CV_PI/180;
		CvSeq* lines = cvHoughLines2(edges, houghStorage, CV_HOUGH_PROBABILISTIC, 
			rho, theta, HOUGH_TRESHOLD, HOUGH_MIN_LINE_LENGTH, HOUGH_MAX_LINE_GAP);

		processLanes(lines, edges, temp_frame);
		*/

		// process vehicles
		vehicleDetection(half_frame, output_frame, cascade, haarStorage);
		drawVehicles(half_frame, output_frame);
		cvShowImage("Output-frame", output_frame);
		cvShowImage("Half-frame", half_frame);
		//cvMoveWindow("Half-frame", half_frame->width*2+10, 0); 

		// show middle line
		//cvLine(temp_frame, cvPoint(frame_size.width/2,0), 
		//	cvPoint(frame_size.width/2,frame_size.height), CV_RGB(255, 255, 0), 1);

		//cvShowImage("Grey", grey);
		//cvShowImage("Edges", edges);
		//cvShowImage("Color", temp_frame);
		
		//cvMoveWindow("Grey", 0, 0); 
		//cvMoveWindow("Edges", 0, frame_size.height+25);
		//cvMoveWindow("Color", 0, 2*(frame_size.height+25)); 

		key_pressed = cvWaitKey(delay);

		if (key_pressed == ' ')
			delay = delay == 1 ? 0 : 1;
	}

	cvReleaseHaarClassifierCascade(&cascade);
	cvReleaseMemStorage(&haarStorage);
	cvReleaseMemStorage(&houghStorage);

	//cvReleaseImage(&grey);
	//cvReleaseImage(&edges);
	//cvReleaseImage(&temp_frame);
	cvReleaseImage(&half_frame);
	cvReleaseImage(&output_frame);

	cvReleaseCapture(&input_video);

	freeVehicles();

	saveConfig();
}
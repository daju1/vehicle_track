#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "CompressiveTracker.h"
#include <list>
#include <map>

CvHaarClassifierCascade *cascade;
CvMemStorage            *storage;

void detect(IplImage *img, IplImage *gray);

struct CompTracker
{
	Rect box;
	CompressiveTracker ct;
	int life_time;

	CompTracker(Mat& gray, Rect r)
	{
		box = r;
		ct.init(gray, box);
		life_time = 0;
	}

	virtual ~CompTracker()
	{
	}

	void processFrame(Mat& gray, float& radioMax)
	{
		ct.processFrame(gray, box, radioMax);
		++life_time;
	}

	bool itentical(Mat& gray, Rect rHaar)
	{
		cv::Rect rect_intersect = box & rHaar; // (rectangle intersection)

		bool _identical = false;
		if (rect_intersect == box) // ������������� ������� ��������� ������ �������������� ���������� �� ��������� �����
		{
			// ����� ������ �� ������, ������� ������ ���������
			life_time = 0;
			_identical = true;
		}
		else if (rect_intersect == rHaar) // ������������� ����� ��������� ������ �������������� �������
		{
			// ������ ����� ������ � �������� �������������� ������ �������������� �����
			box = rHaar;
			ct.init(gray, box);
			life_time = 0;
		}
		else
		{
			double part = rect_intersect.area() / double(box.area());
			_identical = part > 0.5;
			if (_identical)
				life_time = 0;
		}
		
		return _identical;
	}
};



std::list<CompTracker> cts;
//std::map<Rect, CompTracker> ctm;

int main(int argc, char** argv)
{
  CvCapture *capture;
  IplImage  *frame;
  IplImage  *gray;
  int input_resize_percent = 100;
  
  if(argc < 3)
  {
    std::cout << "Usage " << argv[0] << " cascade.xml video.avi" << std::endl;
    return 0;
  }

  if(argc == 4)
  {
    input_resize_percent = atoi(argv[3]);
    std::cout << "Resizing to: " << input_resize_percent << "%" << std::endl;
  }

  cascade = (CvHaarClassifierCascade*) cvLoad(argv[1], 0, 0, 0);
  storage = cvCreateMemStorage(0);
  capture = cvCaptureFromAVI(argv[2]);

  assert(cascade && storage && capture);

  cvNamedWindow("video", 1);

  IplImage* frame1 = cvQueryFrame(capture);
  frame = cvCreateImage(cvSize((int)((frame1->width*input_resize_percent) / 100), (int)((frame1->height*input_resize_percent) / 100)), frame1->depth, frame1->nChannels);
  gray = cvCreateImage(cvSize((int)((frame1->width*input_resize_percent) / 100), (int)((frame1->height*input_resize_percent) / 100)), IPL_DEPTH_8U, 1);

  const int KEY_SPACE  = 32;
  const int KEY_ESC    = 27;

  int frame_number = 1;

  int delay = 1;
  int key = 0;
  do
  {
    frame1 = cvQueryFrame(capture);

    if(!frame1)
      break;

    cvResize(frame1, frame);
	cvCvtColor(frame, gray, CV_BGR2GRAY);
	{
		std::list<CompTracker>::iterator it = cts.begin();
		std::list<CompTracker>::iterator end = cts.end();

		for (; it != end; ++it)
		{
			float radioMax;
			(*it).processFrame(Mat(gray), radioMax);

			char text[255];
			sprintf_s(text, "%d", (*it).life_time, radioMax);
			int fontFace = CV_FONT_HERSHEY_COMPLEX_SMALL;
			int thickness = 1;
			double fontScale = 0.8;
			CvFont font;
			cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX_SMALL, fontScale, fontScale);
			cvPutText(frame, text, cvPoint((*it).box.x, (*it).box.y + (*it).box.height), &font, CV_RGB(255, 0, 0));

			cvRectangle(frame,
				cvPoint((*it).box.x, (*it).box.y),
				cvPoint((*it).box.x + (*it).box.width, (*it).box.y + (*it).box.height),
				CV_RGB(255, 0, 0), 2, 8, 0);

		}
	}

	{

		std::list<CompTracker>::iterator it = cts.begin();
		std::list<CompTracker>::iterator end = cts.end();

		for (; it != end; ++it)
		{
			bool to_erase_current =
				(*it).life_time > 5
				|| (*it).box.x <= 0
				|| (*it).box.y <= 0
				|| (*it).box.x + (*it).box.width >= frame->width
				|| (*it).box.y + (*it).box.height >= frame->height
				;

			if (to_erase_current)
				it = cts.erase(it);
		}
	}

	{
		std::list<CompTracker>::iterator it = cts.begin();
		std::list<CompTracker>::iterator end = cts.end();

		for (; it != end; ++it)
		{
			bool to_erase = false;

			std::list<CompTracker>::iterator it2 = cts.begin();
			std::list<CompTracker>::iterator end2 = cts.end();

			for (; it2 != end2; ++it2)
			{
				Rect intersect = (*it).box & (*it2).box;
				double min_area = min((*it).box.area(), (*it2).box.area());
				double part = double(intersect.area()) / min_area;

				to_erase = part > 0.5 && (*it).box.area() > min_area;
				if (to_erase)
					break;
			}

			if (to_erase)
				it = cts.erase(it);
		}
	}

	//if (frame_number == 1)
	detect(frame, gray);

	cvShowImage("video", frame);

	key = cvWaitKey(delay);	

    if(key == KEY_SPACE)
		delay = delay == 1 ? 0 : 1;

    if(key == KEY_ESC)
      break;

	++frame_number;

  }
  while(1);

  cvDestroyAllWindows();
  cvReleaseImage(&frame);
  cvReleaseCapture(&capture);
  cvReleaseHaarClassifierCascade(&cascade);
  cvReleaseMemStorage(&storage);

  return 0;
}

void detect(IplImage *img, IplImage *gray)
{
  CvSize img_size = cvGetSize(img);
  CvSeq *object = cvHaarDetectObjects(
	  img,
	  cascade,
	  storage,
	  1.05, //1.1,//1.5, //-------------------SCALE FACTOR
	  2, //2        //------------------MIN NEIGHBOURS
	  CV_HAAR_DO_CANNY_PRUNING
	  );
    /*cvSize(0,0),//cvSize( 30,30), // ------MINSIZE
    img_size //cvSize(70,70)//cvSize(640,480)  //---------MAXSIZE
    );*/

  std::cout << "Total: " << object->total << " cars" << std::endl;
  for(int i = 0 ; i < ( object ? object->total : 0 ) ; i++)
  {
    CvRect *r = (CvRect*)cvGetSeqElem(object, i);

	cvRectangle(img,
		cvPoint(r->x, r->y),
		cvPoint(r->x + r->width, r->y + r->height),
		CV_RGB(0, 0, 255), 2, 8, 0);


	// find identical
	bool found_identical = false;

	std::list<CompTracker>::iterator it = cts.begin();
	std::list<CompTracker>::iterator end = cts.end();

	for (; it != end; ++it)
	{
		if ((*it).itentical(Mat(gray), *r))
		{
			found_identical = true;
			break;
		}
	}

	if (found_identical)
		continue;

	cts.push_back(CompTracker(Mat(gray), Rect(*r)));
  }

}